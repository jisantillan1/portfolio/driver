#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/string.h>
#include <linux/uaccess.h>        
#define DEVICE_NAME "JAVIER_chardevice"
#define BUFFER 100
 int init_module(void);
 void cleanup_module(void);
static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char *, size_t, loff_t *);

char mensaje [BUFFER];
char *p_mensaje;
int major ;
int ret ;
int device_abierto = 0 ;  // 0 = device cerrado
			 //  1 = device abierto

static struct file_operations fops = {
    	.read = device_read,
        .write = device_write,
        .open = device_open,
        .release = device_release
};

int init_module(void){
	major = register_chrdev(0, DEVICE_NAME, &fops);
        if (major < 0) {
		 printk(KERN_ALERT "JAVIER : Ha ocurrido una falla en el registro del char device\n");
		 return major;
	}
	printk(KERN_INFO " JAVIER : Driver registrado \n");
	printk(KERN_INFO "JAVIER : Se asigno el major number %d\n", major);
	return 0;
}

void cleanup_module(void){        /*        Desregistramos el dispostivo          */
	unregister_chrdev(major, DEVICE_NAME);
	printk(KERN_INFO " JAVIER : Driver desregistrado \n");
}

static int device_open(struct inode *inode, struct file *file){
        if (device_abierto){
		printk(KERN_ALERT "JAVIER: El device ya se encuentra abierto");
                return -1;
	}
    device_abierto++;
     p_mensaje = mensaje;
	printk(KERN_INFO "JAVIER: Se ha abierto correctamente el device");
        return 0;
}
static int device_release(struct inode *inode, struct file *file){
    device_abierto--;
	printk(KERN_INFO "JAVIER: Se ha cerrado el device");
        return 0;
}

static ssize_t device_read(struct file *filp,char *buffer,size_t length,loff_t * offset){
   int bytes_leidos = 0;
   printk(KERN_INFO "Se ha leido el dispositivo");	
   if (*p_mensaje == 0) // Si el mensaje esta vacio no hacemos nada
        return 0;
    while (length && *p_mensaje) {   
        
        put_user(*(p_mensaje++), buffer++); // Copiamos los datos del kernel al usuario
        length--;
        bytes_leidos++;
        
    }

    return bytes_leidos; // Retornamos los bytes leidos
}

void cifrado_caesar(char * mensaje, int shift ){
    int i = 0;
    while(mensaje [i]){
    char caracter = mensaje[i];
        if (caracter >= 'A' &&  caracter<='Z' ) {
            caracter = ((caracter+shift-65)%26) +65; // mayusculas en ascii 65 a 90 (26 letras) hacemos esa cuenta 
            mensaje[i] = caracter;		   // para no pasarnos 	
            i++;}
        else {                                        
            if (caracter >= 'a' && caracter <= 'z')
            {
            caracter = ((caracter+shift-97)%26) +97; // minsculas 97 a 122
            mensaje[i] = caracter;
            i++;
            }
            else
            {
            mensaje[i] = caracter;
            i++;
            }
        }    
    }   
}

static ssize_t device_write(struct file * filp,const char * buffer,size_t length,loff_t * offset){
    int i;
    for (i = 0; (i < length) && (i < BUFFER); i++){
        get_user(mensaje[i], buffer + i); // tomamos los datos del usuario al nuestro vector en el kernel
    }
    
    cifrado_caesar(mensaje,4); // aplicamos cifrado_caesar al mensaje con un shift = 4

    p_mensaje = mensaje;
    printk(KERN_INFO "Se ha escrito y encriptado el dispositivo con un shift = 4");	
   
    return i; // Utilizamos el indice i para retornar cuanto escribimos
}
  
MODULE_LICENSE("GPL ");
MODULE_AUTHOR(" JAVIER");
MODULE_DESCRIPTION("Un primer driver ");



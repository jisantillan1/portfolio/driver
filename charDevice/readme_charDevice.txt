Ejecutamos en una terminal linux donde tengamos nuestro Makefile y chardevice.c :

$ make //Ejecutamos make para generar nuestro chardevice.ko
$ sudo insmod chardevice.ko
$ dmesg //Vemos que nuestro driver se haya registrado correctamente y tomamos el valor del major number generado (239 en este caso)
$ cd /dev
$ sudo mknod device c 239 0
$ sudo chmod 777 device //Cambiamos permisos de nuestro device 
$ echo "mensaje" > device //Escribimos nuestro mensaje a cifrar
$ cat device //Leemos
$ dmesg //Vemos que ocurre en el kernel 
$ sudo rmmod chardevice // Desregistramos chardevice y device
$ sudo rm device	//





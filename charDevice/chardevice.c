#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/uaccess.h>        
#define DEVICE_NAME "JAVIER_chardevice"
#define BUFFER 100
 int init_module(void);
 void cleanup_module(void);
static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char *, size_t, loff_t *);

char data [BUFFER];
int major ;
int ret ;
int device_abierto = 0 ;  // 0 = device cerrado
			 //  1 = device abierto

static struct file_operations fops = {
	.read = device_read,
        .write = device_write,
        .open = device_open,
        .release = device_release
};

int init_module(void){
	major = register_chrdev(0, DEVICE_NAME, &fops);
        if (major < 0) {
		 printk(KERN_ALERT "JAVIER : Ha ocurrido una falla en el registro del char device\n");
		 return major;
	}
	printk(KERN_INFO " JAVIER : Driver registrado \n");
	printk(KERN_INFO "JAVIER : Se asigno el major number %d\n", major);
	return 0;
}

void cleanup_module(void){        /*          * Unregister the device          */
	unregister_chrdev(major, DEVICE_NAME);
	printk(KERN_INFO " JAVIER : Driver desregistrado \n");
}

static int device_open(struct inode *inode, struct file *file){
       // static int counter = 0;
        if (device_abierto){
		printk(KERN_ALERT "JAVIER: El device ya se encuentra abierto");
                return -1;
	}
    device_abierto++;
	printk(KERN_INFO "JAVIER: Se ha abierto correctamente el device");
        return 0;
}
static int device_release(struct inode *inode, struct file *file){
    device_abierto--;
	printk(KERN_INFO "JAVIER: Se ha cerrado el device");
        return 0;
}

static ssize_t device_read(struct file *filp,char *buffer,size_t length,loff_t * offset){
    // encriptacion
    int i ;
    for ( i= 0;  i<BUFFER;  i++){

    	data[i] = data[i] + 2;
    }
    int maxbytes;// bytes maximos que pueden ser leidos
    int bytes_to_read;//  numero de bytes a leer
    int bytes_read;// bytes actualmente leidos
    maxbytes = BUFFER - *offset;
    if(maxbytes > length) {
        bytes_to_read = length;}
    else{
        bytes_to_read = maxbytes;}
   
    
    bytes_read = bytes_to_read - copy_to_user(buffer, data + *offset, bytes_to_read);
    *offset += bytes_read;
    printk(KERN_INFO "JAVIER: el dispostivo ha sido leido\n");
    return bytes_read;

}
static ssize_t device_write(struct file * filp,const char * buffer,size_t length,loff_t * offset){
    int maxbytes; 
    int bytes_to_write; // numero de bytes a escribir
    int bytes_writen;//bytes actualmene leidos
    maxbytes = BUFFER - *offset;
    if(maxbytes > length) 
        bytes_to_write = length;
    else
        bytes_to_write = maxbytes;
    bytes_writen = bytes_to_write - copy_from_user(data + *offset, buffer, bytes_to_write);
    *offset += bytes_writen;
    printk(KERN_INFO "JAVIER : el dispositivo ha sido escrito\n");
    return bytes_writen;
}
MODULE_LICENSE("GPL ");
MODULE_AUTHOR(" JAVIER");
MODULE_DESCRIPTION("Un primer driver ");


